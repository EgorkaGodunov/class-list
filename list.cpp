#include <iostream>

using namespace std;

struct Array{
    int *A;
    int size;
    int length=0;


    void create(int size){
        A = (int *) malloc(size * sizeof(int));

        length = 0;
    };

    void display(){
        for(int i=0; i<length; i++){
            cout << A[i] <<" ";
        }
        cout << endl;
    }
    void append(int value){
        if(length < size){
            A[length]= value;
            length++;
        }
    }

    void insert(int index, int value){
        if(index >=0 && index <length){
            for(int i=length;i>index;i--){
                A[i]=A[i-1];
            }
            A[index]=value;
            length++;
        }
    }

    int del(int index){
        int x = 0;
        if(index >=0 && index < length){
            x = A[index];
            for(int i = index;i<length-1;i++){
                A[i]=A[i+1];
            }
            length--;
            return x;
        }
        return -1;
    }

    int search(int value){
        for(int i=0;i<length;i++){
            if(A[i]==value){
                return i;
            }
        }

        return -1;
    }
    
    
    
    void sort(){
        for(int i=0;i<length;i++){
            for(int j=0;j<length-i-1;j++){
                if(A[j]>A[j+1]){
                    swap(A[j],A[j+1]);
                }
            }
        }
    }
    
    

    void clear(){
        length=0;
    }
    void reverse(){
        int *B = (int *) malloc(length *sizeof(int));
        int i,j;
        for(i=length-1, j=0,i>=0;i--;j++){
            B[j]=A[i];
        }
        for(int i=0;i<length;i++){
             A[i]=B[i];
         }   
    }
    
    
        void Merge(int first, int last){
            int middle, start, final, j;
            int *mas=new int[100];
            middle=(first+last)/2;
            start=first;
            final=middle+1;
            for(j=first; j<=last; j++)
                if ((start<=middle) && ((final>last) || (A[start]<A[final]))){
                    mas[j]=A[start];
                    start++;
                }else{
                    mas[j]=A[final];
                    final++;
                }
            for (j=first; j<=last; j++) A[j]=mas[j];
            delete[]mas;
        };
        void MergeSort(int first, int last){
            if (first<last){
                MergeSort(first, (first+last)/2);
                MergeSort((first+last)/2+1, last);
                Merge(first, last);
            }
    
        };



};

int main(){
    
    Array arr;
    arr.size=18;
    arr.create(100);
    arr.append(2);
    arr.append(6);
    arr.append(9);
    arr.append(2);
    arr.append(65);
    arr.append(93);
    arr.append(22);
    arr.append(69);
    arr.append(79);
    arr.append(42);
    arr.append(67);
    arr.append(99);
    arr.append(213);
    arr.append(64);
    arr.append(943);
    arr.append(211);
    arr.append(622);
    arr.append(933);
    
    
    int len = arr.length;
    arr.display();
    arr.MergeSort(1,len);
    arr.display();
    
    return 0;
}
